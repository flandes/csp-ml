


TP4.2 == this website:
- http://www.adeveloperdiary.com/data-science/deep-learning/neural-network-with-softmax-in-python/


a careful check that a simple regression on 4 points performs EXACTLY the same computations as its pytorch counterpart:
- https://stackoverflow.com/questions/54247143/what-is-the-difference-between-mlp-implementation-from-scratch-and-in-pytorch





In torch:

Very concrete example of autograd, on a simple polynomial model, just to show that it computes gradients for you:
- https://pytorch.org/tutorials/beginner/pytorch_with_examples.html#warm-up-numpy

Each torch line is heavily commented, even more so than in my TP5.1 I think.
(it's a tutorial but reported inside the official doc, so it also tells how you define your own functions, e.g. your own activation function)


Autograd explained with nice visuals (a bit lengthy, but the few equations + visual sketch, are nice)
- https://blog.paperspace.com/pytorch-101-understanding-graphs-and-automatic-differentiation/

A simple MLP, with torch calls commented, but not as much as in my TP5.1:
- https://github.com/christianversloot/machine-learning-articles/blob/main/creating-a-multilayer-perceptron-with-pytorch-and-lightning.md


