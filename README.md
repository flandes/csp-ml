# PCS-ML

Repo related to the Machine Learning class in the Physics of Complex Systems Master program.


My email: francois.landes -- you got it -- universite-paris-saclay dot fr

Register for the project: (2024-25)
https://docs.google.com/spreadsheets/d/12bVhZ0QXRdfYcLgXz7wToyMQ0oasfCePDSKBFKwOYUo/edit?usp=sharing

### Project presentatino date:
A priori, it will be on Friday, March 21st. (TO BE CONFIRMED)

#### Little Warning:
Although the exam consists in presenting one of the topics I propose,
corresponding typically to one or a couple of papers to read/summarize/re-code/etc (depending on the topic),
a large fraction of the grade comes from your ability at answering my questions during the discussion time.
I ask questions assuming that you have followed the classes. It happens very often that I ask for further
clarifications on a technical point of the paper, which corresponds to a concept seen in class.
So, although in principle being solid on your reading is enough to get a good mark,
typically, having followed all classes can help a lot improvising a (good) answer,
when it happens that you overlooked a concept during the preparation of your presentation.


### Datasets

Datasets are in another gitlab project: ttps://gitlab.inria.fr/flandes/data-for-teaching

#  Tentative outline of the class:

### session 1 : Linear regression, perceptron.
- discussing scientific programming, python, jit (numba), GPGPUs, vectorization.
- what is ML / what you know about it (inverted class).
- Lecture: "Linear" Regression (session 1, no priors, no MLE/MAP).
- Lecture: Learning (also) by Gradient Descent.
- Lecture: Perceptron (a few bad losses, and then a good one or two).

#### TD:
- Code your perceptron from scratch for (binary)-classification of digits.



### session 2: Overfitting (recap), MLP idea, multi-class classif
- Lecture: Overfitting recap,
- Bootstrapping !
- Lecture: 1-neuron Neural Network (Perceptron, etc).
- Logistic Regression as well: same story.
- MLP idea
- The multi-class classification.

#### TD:
- multi-class classification, from scratch.


#### Stuff planned but that we will not cover (ever), after all:

- showing examples of overfirring ≃ large weights

### session 3: Naive Bayesian (supervised) and EM (unsupervised) algorithms

- first, feature maps and Kernels (Kernelized perceptron)
- Lecture: Naive Bayes and EM, from scratch (MLE)
- Plus a bit of the MAP flavor (e.g. for text classif.) See the doc of Sklearn, they talk about priors.. now you see the point.

#### TD:

- Code the Naive Bayes for MNIST
- Code the EM (Bernoulli mix) for Mnist, again. Compute perf with minimal number of avaialble labels, and optimize hyper-param K.
- Bonus: also do PCA+full cov. matrix (e.g. with Gaussians) [sorry for this one I didn't finish up the correction]


### Session 4: MLP, from scratch and then with pytorch

- EM recap:
    - Correction of EM algo: live coding, ideally.
    - Summary of GMM/NaiveBayes/EM + Conclusion: we built the simplest Generative models
    - Correction of Naive Bayesian (Bernoulli) model: play with PCA/feature maps to reduce/expand data. Can also do PCA then feature maps !]
    - pipeline example (cleaned): show the tool `Pipeline` of sklearn, and compare d=1 and d=2 feature maps for PCA+NaiveGaussian (d=1 can work better !)
    [- use full-covariance matrix Gaussian model... or feature maps ]


#### TD: MLPs with numpy !
- TP4.1 - First, simple regression in numpy:
    + derive the updates (backpropagation) from scratch (partly like a lecture)
    + then implement it, using my notations
- Bonus - if time allows - for multiclass-classification, look at a working code (more generic one), try it on MNIST
    (this one is somehow closer to the Pytorch logic, but also more cumbersome to read)

#### Note for myself - points to remember (feedback from students):
- use notation dW, db when doing the mathematics derivations, for simpler consistency
- give away the formula with explicit matrix forms, etc, at least for dW3, db3, sooner (when coding it) / discuss in detail the matrix formulation (with (N,) of each update (?))
- overall, a bit less time on struggling with the code implementation (which will happen by itself if I give the above formula)
- overall, a good (important) show of what is the backpropagation ! :)


### session 5: a bit of Deep -- MLPs, CNNs
- Lecture: Basics of pytorch (global logic (OOP), backprop, computational graph, etc)
- TP: MLP with pytorch
    + TP5.1: use a simple MLP to classify CIFAR-10. A lot of the code is provided already. Goal: get familiar with pytorch's logic
    + TP5.2: try learning the identity function! It's not so easy. This is an opportunity to put your hands into the (pytorch) MLP a little bit.
- Lecture: basics of Convolutional Layers
    + weight sharing, convolution, special case: Convolution 1x1
    + translational equivariance
    + boundaries: various possib.
    + coarse-grainig: maxpool and padding, strides
- TP: CNN in pytorch
    + TP5.3: code a CNN ! You only have to code the forward pass. Again, an opportunity to play with/learn about pytorch.
    + [optional: load and use pre-trained VGG net]
- Lecture: advanced CNN topics
    + ResNet, AlexNet, VGGNet, skip-connections
    + Data Augmentation
    + maybe: dropout



### session 6: probably: AutoEncoders (or, NLP ?)

- Lecture:
    + PCA as dimensional reduction - link with AutoEncoders
    + NLP basics, encodings, embeddings (word2vec)
- TP6.1 : port your CPU-only CNN (TP5.3) to the GPU (on collab you have a GPU)
- TP6.2 : train your own word embedding (word2vec)
- TP6.3 : play with a well-trained embedding (more a demo than a TP)
- Lecture (depending on remaining time): advanced CNN topics, such as:
    + see https://www.lri.fr/~gcharpia/deeppractice/index_2023.html
    + U-Net (deconvolutions, up-sampling)
    + AutoEncoders
    + GNNs
    + Attention
    + maybe (again?): transfer learning, Few-shot, One-shot learning
    + a good read: https://www.deeplearningbook.org/contents/convnets.html
    + Lecture-conclusion: transfer learning, representation learning, generality of self-supervised learning
    + [for project: Siamese networks, triplet loss (constrastive)]


### Alternative ideas for session 6 (for next year)

- to consider, next year? - simple exercise with 2D points data, low N, with random or almost random labels. Observe the overfitting patterns, depending on MLP complexity. (here we would not look at test set, just study the expressive power of MLP, or shattering power)

- or AE
    - AE idea
    - coding your own AE
    - use it as generative model
    - transfer learning

### session 7: EXAM !
- selected papers explanation - focusing on new methods' papers.







