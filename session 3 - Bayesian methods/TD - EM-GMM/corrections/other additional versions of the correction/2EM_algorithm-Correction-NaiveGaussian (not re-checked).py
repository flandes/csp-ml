#!/usr/bin/env python
# coding: utf-8

# # Note:
# this is a fraft correction
# 
# ## take home messages for Gaussian model:
# 
# - it does not work well on Mnist, the model is not appropriate
# - on the PCA components of MNIST, at least there are not NaNs everywhere
# - yet, clusters end up being empty (for many ot them)
# - I was too lazy to do the full-covariance case..  (treat pbms one at a time is a good idea)
# 
# ### TODO:
# use EM on an actual mixture of Gaussians -> and visualize (but: this is has been done thousands of time on the Internet..)

# In[1]:


import numpy as np
import matplotlib
import matplotlib.pyplot as plt 


K=10

## we load the whole data set at once, so as to manipulate it directly (using numpy arrays)
## for very large data sets, one needs to read on-the-fly (at least at production time, not at the debugging stage)

import numpy as np
data = np.load('/home/flandes/data/mnist70.npz', 'X', 'y')
X = data['X']
y = data['y']
print(X.shape)
N = X.shape[0]

## split train an dtest data, to avoid inadvertently cheating.
unlabelled_dataset = X.copy()
labels_for_final_accuracy_measurement = y.copy()
del y


## compute the proba of an image xi, 
## given its cluster k and parameters theta 
def compute_log_P_Xi_given_k_and_theta_Gaussian(xi, k, theta):
    mu = theta[0]
    sigmas = theta[1]
    D = X.shape[1]
    logP = - 0.5 * (xi- mu[k])**2 @(sigmas[k]**-2)  -D/2 *np.log(2 *np.pi) - np.sum(np.log(sigmas[k]))
    return logP


import sklearn.decomposition
PCA = sklearn.decomposition.PCA(n_components=20)
PCA.fit(X)
XGaussian = PCA.transform(X)
X = XGaussian
D = XGaussian.shape[1]

print(XGaussian.shape)

import sklearn.cluster
kmeans = sklearn.cluster.KMeans(n_clusters=K, random_state=0, n_init="auto")
kmeans.fit(X)

affectations = np.zeros((K, N))
for n in range(N):
    k = kmeans.labels_[n]
    affectations[k ,n] = 1    
classFrequencies = np.sum(affectations, axis=1)/N

# mu = np.random.normal(0,1,(K,D))
mu = kmeans.cluster_centers_
sigma = np.random.random((K,D))
for k in range(K):
    sigma[k] = ((affectations[k] @ ((X-mu[k])**2) ) / classFrequencies[k]/N )

# theta = [mu,sigma]
# # affectations = np.zeros((K, N))
# classFrequencies = np.random.random(K)
# classFrequencies /= classFrequencies.sum()


MaxIt=10
## main loop ##
for iteration in range(0,MaxIt,1):
    

    ######################
    ## step M (Maximization -- of the log-likelihood)
    ## i.e. update of "pi_k, theta_k" (classFrequencies, parameters:mu.sigma,etc )  ##
    
    ## update pi_k:
    classFrequencies = np.sum(affectations, axis=1)/N
    print(np.round(classFrequencies,3))
    ## update theta[k,j]:  (using the very efficient numpy matrix product)
    mu = (affectations @ X / affectations.sum(axis=1, keepdims=True))  ## shape (K,D)
    for k in range(K):
        sigma[k] = ((affectations[k] @ ((X-mu[k])**2) ) / classFrequencies[k]/N )
    theta = [mu, sigma]
            

    ######################
    ## step E (Expectation): update of "a_ik" (affectations) ##
    
    ### semi-numpy version: (much slower)
    # for i in range(Nex):
    #     xi = dataset[i]
    #     for k in range(K):
    #         temporary = compute_P_Xi_given_k_and_theta(xi,k, theta)
    #         affectations[k,i] = classFrequencies[k]*temporary
    #     affectations[:,i] /= np.sum(affectations[:,i]) ## divide by the total (denominator)
    
    ### fully-numpy version : (much faster)
    logLikelihoods = np.zeros((K,N))
    for k in range(K):
        logLikelihoods[k] = compute_log_P_Xi_given_k_and_theta_Gaussian(X,k, theta)
        affectations[k] = np.exp( \
                                 np.log(classFrequencies[k]) \
                                 + logLikelihoods[k]\
                                +1e-16 )
    affectations /= np.sum(affectations,axis=0) ## normalize teh a's for each sample
    
    
    ## monitoring of the quality of the clustering ##
    AvgLogLikelihood = np.max(logLikelihoods, axis=0).mean() # np.log(np.max(affectations, axis=0)) 
    print(AvgLogLikelihood)
    
            
##########################################################################


# In[ ]:




