#!/usr/bin/env python
# coding: utf-8

# **EM Algorithm** (Expectation - Maximization)
# 
# We're going to implement the EM algorithm for a mixture of Bernoullis
# 
# The Expectation-Maximization algo. is used in sk-learn, for instance in GMMs: http://scikit-learn.org/stable/modules/mixture.html#estimation-algorithm-expectation-maximization
# 
# Additional notes are available here: https://allauzen.github.io/articles/MixturesAndEM/

# 
# ## Think for a bit
# 
# Is EM a supervised or unsupervised learning algorithm ? 
# 
# What kind of data set is MNIST ? 
# 
# Then, what are we going to do ?  How is this called ?
# 
# 

# 
# ## Implement the EM algo. for a mixture of Bernoulli (laws)
# - The cluster number K should be an argument of the function
# - A maximum number of iterations, *MaxIt*, should act as stopping condition
# - During the E step, compute and *store* the log-likelihood of the data, so as to monitor its evolution along iterations (~epochs)
# 
# Apply the algorithm on MNIST:
# - try out K=5,10,15
# - Visualize the images coresponding to each cluster's paraemeters. Would that be as straightforward in a Gaussian model (visualizing all the model's parameters?)

# In[1]:


import numpy as np
import matplotlib.pyplot as plt 
import seaborn as sns

import sklearn
import sklearn.cluster
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.decomposition import PCA
np.random.seed(42)


# In[3]:


## we load the whole data set at once, so as to manipulate it directly (using numpy arrays)
## for very large data sets, one needs to read on-the-fly (at least at production time, not at the debugging stage)

# wget https://gitlab.inria.fr/flandes/data-for-teaching/-/raw/master/ManualLoad.npz
data = np.load('ManualLoad.npz', 'X', 'y')
X = data['X'] / 255.0
y = data['y']

X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2, random_state=42)

n_components=30
K = 10
n_iter=50
EPS=1e-6

# Now, let's monitor something more interpretable than the log-likelihood... what could it be ? 
# 
# Trick: you may use the functions np.argmax() and np.bincount(), and "cheat", using the labels from the examples
# 
# **Question**: would this strategy be possible for a purely unsupervised task ?

# Now add this interesting metric into your function, and add some plot of it from the function as well (possibly along wth the log-likelihood plot)

# In[ ]:


X_train.shape


# ### GMM to MNIST Data Set


#pca fitted on the training set
pca = PCA(n_components=n_components, random_state=42)
X_train = pca.fit_transform(X_train)
X_val = pca.transform(X_val)


# In[5]:


N,D = X_train.shape
print("N = ",N)
print("D = ",D)

def gaussian_pdf_multi_points(x, mean, cov, cov_inv, det):
    D = x.shape[1]
    diff = x - mean ## shape (N,D)  (for each k)
    # cov_inv = np.linalg.inv(cov) ## avoid repeated request by computing it once, outside of the loop
    # -1/2 * (x - mu)^T * inv(Sigma) * (x - mu)
    exponent = -0.5 * ((diff @ cov_inv) * diff).sum(axis=1)
    denom = np.sqrt((2 * np.pi)**D * det)
    pdf_val = np.exp(exponent) / denom
    return pdf_val

def em_gmm(X_train, X_val, K=10, n_iter=3):
    #dimensions from X_train
    N, D = X_train.shape
    Nval, D = X_val.shape
    affectations = np.zeros((K, N))
    affectations_val = np.zeros((K, Nval))

    ##intialiszation: trick: intialize to K-means
    kmeans = sklearn.cluster.KMeans(n_clusters=K, random_state=0, n_init="auto")
    kmeans.fit(X)
    for n in range(N):
        k = kmeans.labels_[n]
        affectations[k ,n] = 1
    affectations /= affectations.sum(axis=0) ##sum over K is 1
    classFrequencies = np.sum(affectations, axis=1)/N  ## sum over N is of shape (K,)
    classFrequencies /= classFrequencies.sum()
    print("classFrequencies.sum()  (should be 1)", classFrequencies.sum())
    print(classFrequencies)

    log_like_train = []
    log_like_val = []

    for it in range(n_iter):
        print("Iteration:", it)

        # Maximization step
        means = affectations @ X_train
        means /= (affectations.sum(axis=1).reshape(-1,1) + EPS)

        covar = np.zeros((K, D, D))
        for k in range(K):
            # Update covar matrices, outer products of differences
            diff = X_train - means[k]  ## shape is (N,D)
            covar[k] = (diff.T * affectations[k,:].T) @ diff
            covar[k] /= (affectations[k].sum() + EPS)
            covar[k] += EPS * np.eye(D)


        # Expectation step
        for k in range(K):
            cov_inv = np.linalg.inv(covar[k])
            det = np.linalg.det(covar[k])
            prob = gaussian_pdf_multi_points(X_train, means[k], covar[k], cov_inv, det)
            affectations[k, :] = classFrequencies[k]* prob.copy()

        ## compute train likelihood: basically just the sum of probas (with the class frequencies) but BEFORE normalization
        train_logLikelihood = np.log(affectations.sum() + 1e-10)
        log_like_train.append(train_logLikelihood)

        # Expectation step (for validation step, only for computing validatoin log likelihood)
        for k in range(K):
            cov_inv = np.linalg.inv(covar[k])
            det = np.linalg.det(covar[k])
            prob = gaussian_pdf_multi_points(X_val, means[k], covar[k], cov_inv, det)
            affectations_val[k, :] = classFrequencies[k] * prob.copy()
        val_logLikelihood = np.log(affectations_val.sum() + 1e-10)
        log_like_val.append(val_logLikelihood)

        affectations /= affectations.sum(axis=0)  ## sum over k
        classFrequencies = affectations.sum(axis=1)/N #  # sum along data points for given k
        classFrequencies /= classFrequencies.sum() ## to make really sure it sums to 1.

        print("training log likelihood:", train_logLikelihood)
        print("validation log likelihood:", val_logLikelihood)
        print()

    return classFrequencies, means, covar, log_like_train, log_like_val

classFrequencies, means, covar, L_train, L_val = em_gmm(X_train, X_val, K, n_iter)


plt.plot(L_train)
plt.plot(L_val)
plt.show()


def predict(X, classFrequencies, means, covar):
    N = X.shape[0]
    affectations = np.zeros((K,N))
    for k in range(K):
        cov_inv = np.linalg.inv(covar[k])
        det = np.linalg.det(covar[k])
        prob = gaussian_pdf_multi_points(X, means[k], covar[k], cov_inv, det)
        affectations[k, :] = classFrequencies[k] * prob
    preds = np.argmax(affectations, axis=0)
    return preds, affectations

preds, a = predict(X_val, classFrequencies, means, covar)
cm_train = sklearn.metrics.confusion_matrix(y_val, preds)
class_labels = [str(i) for i in range(cm_train.shape[0])]
plt.figure(figsize=(8, 6))
sns.heatmap(cm_train, annot=True, fmt='d', cmap='Blues',
            xticklabels=class_labels, yticklabels=class_labels)
plt.xlabel('Predicted Label', fontsize=12)
plt.ylabel('True Label', fontsize=12)
plt.title('Confusion Matrix', fontsize=14)
plt.savefig("cm_handmade.png")
plt.show()



import sklearn.mixture
import sklearn.metrics
gmm = sklearn.mixture.GaussianMixture(n_components=K, covariance_type='full', tol=0.001, reg_covar=EPS,\
                                      max_iter=100, n_init=1, init_params='kmeans', weights_init=None, means_init=None, \
                                      precisions_init=None,random_state=None, warm_start=False, verbose=0,\
                                      verbose_interval=10)
gmm.fit(X_val)
cm_train = sklearn.metrics.confusion_matrix(y_val, gmm.predict(X_val))
class_labels = [str(i) for i in range(cm_train.shape[0])]
plt.figure(figsize=(8, 6))
sns.heatmap(cm_train, annot=True, fmt='d', cmap='Blues',
            xticklabels=class_labels, yticklabels=class_labels)
plt.xlabel('Predicted Label', fontsize=12)
plt.ylabel('True Label', fontsize=12)
plt.title('Confusion Matrix', fontsize=14)
plt.savefig("cm_sklearn.png")
plt.show()







