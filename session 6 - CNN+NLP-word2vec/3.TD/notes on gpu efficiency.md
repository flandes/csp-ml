
# GPU efficiency
==================

## GPU efficiency (with the intial angle of caring about data loading - which is easily a bottleneck !)

- Note: make sure your data is on a SSD rather than HDD for faster reading.
    Or, if your filesystem is the good one, it won't matter (with enough workers to load from disks in //)


The standard doc about data loading (where to start from):
- https://pytorch.org/docs/master/data.html

Also, concise best practices tips from IDRIS (French stuff !)
http://www.idris.fr/eng/jean-zay/gpu/jean-zay-gpu-torch-data-preprocessing-eng.html
+ many other good resources for optimization on that website

very very good post on tools to optimize GPU efficiency (memory/speed optimization):
- (somehow, a bit too advanced, to start)
- https://huggingface.co/docs/transformers/perf_train_gpu_one
- (+ you learn about multiple tricks that exist)

pytorch lightning tips (more dense, quite concise and to the point):
- (a bit too advanced, as well)
- https://lightning.ai/docs/pytorch/stable/advanced/speed.html

--- Stuff I aven't read in full myself, yet: ----

- Lightning : amazing resource to write efficient, SOTA level code.

Tutorial to Lightning:
- https://lightning.ai/pages/community/tutorial/step-by-step-walk-through-of-pytorch-lightning/
A nice example of lightning code:
- https://lightning.ai/docs/pytorch/stable/notebooks/lightning_examples/cifar10-baseline.html
Other tutorials:
- https://lightning.ai/docs/pytorch/stable/tutorials.html

Basic ligntning stuff:
- https://lightning.ai/docs/pytorch/stable/model/train_model_basic.html
all the details of the Lightning Trainer class:
- https://lightning.ai/docs/pytorch/stable/common/trainer.html#trainer-flags


A thread that shows that people are a bit confused on these matters...
- https://discuss.pytorch.org/t/guidelines-for-assigning-num-workers-to-dataloader/813

A way to visualize results (there are many competing softwre for this kind of task):
- MLflow
- Weights&Biases (W&B)
- https://www.tensorflow.org/tensorboard?hl=en
- https://pytorch.org/tutorials/recipes/recipes/tensorboard_with_pytorch.html



