# wget "https://gitlab.inria.fr/flandes/data-for-teaching/-/blob/master/fashion-mnist-reshaped.npz"

## OR:
# %%capture
#!git clone https://github.com/zalandoresearch/fashion-mnist.git


import numpy as np
data = np.load('/home/flandes/data/fashion-mnist-reshaped.npz')
X = data['train_images']
y = data['train_labels']
Xtest = data['test_images']
ytest = data['test_labels']
print(X.shape)
