
- Learning rate - related algos
    + Here you may re-implement a method from scratch. And explain the general ideas (momentum, etc) to the whole class.
    + You may discuss: SGD with momentum, adam, Nesterov-adam, rmsprop, gradient clipping, etc. The point is of course not to list all but summarize key ideas, make it interesting (!)
