# Instructions

- in each section, I put first the subjects I like best.
- if instructions on how to handle (like, what do I expect from you on that topic) are not detailed enough, please let me know. This could also happen that you read quite a lot, and then need more precise instructions. Don't hesitate to ask me by e-mail. I will then narrow down the subject.
- sometimes there is a folder associated to a topic (esp. if papers are behind a paywall, or because I felt like creating a folder). Then there are sometimes some complementary instructions in the folder.
- once your topic is chosen, fill up: https://docs.google.com/spreadsheets/d/12bVhZ0QXRdfYcLgXz7wToyMQ0oasfCePDSKBFKwOYUo/edit?usp=sharing
- if you decide to make a group of 2, check with me that the subject is appropriate (rich enough) + write both your names in the spreadsheet.


## Interesting (Deep) architectures (rather precise topics):

- Rotation Equivariant networks (E(3) or SE(3))
    + The paper in the root folder, *Geometric and Physical Quantities Improve E(3) Equivariant Message Passing*, should be self-consistent. The others may be less clear. You can browse the litterature yourself as well.
    + Here you should understand all and try to summarize to class (disclaimer: I work on this topic)
    + This may be technical (I am not a fair judge)
    + A recent and high-success architecture: *MACE: Higher Order Equivariant Message Passing Neural Networks for Fast and Accurate Force Fields*

- Scale equivariant networks:
    + *Truly Scale-Equivariant Deep Nets with Fourier Layers*
    You may relate what they do with the famous UNet architecture:
        *U-net: Convolutional networks for biomedical image segmentation*
    + Other good references:
        + *Riesz networks: scale invariant neural networks in a single forward pass*
        + *Scale Steerable Filters for Locally Scale-Invariant Convolutional Neural Networks*
        + *Scale-Covariant and Scale-Invariant Gaussian Derivative Networks*
        + *Scale-Invariant Scale-Channel Networks: Deep Networks That Generalise to Previously Unseen Scales*
        + *Scale-Equivariant Steerable Networks*  (a bit cheap in the end, if I recall well)
    + This is somewhat related and complementary to the other subject, "Conditional Probabilistic Generative models + Wavelets".

- LLMs for DNA:
    + *Caduceus: Bi-Directional Equivariant Long-Range DNA Sequence Modeling*
    + Summarize LLMs quickly, and then LLMs for DNA: how are they different ? Similar ? What is the expected outcome (think about transfer learning of course).
    + Ideally, find some sequences, and apply a pre-trained model to them.
    + Or, find an LLm pretrained for amino-acid data and re-do Martin Weigt's project with it ? Here are some papers on protein sequences:
        + *VespaG: Expert-guided protein Language Models enable accurate and blazingly fast fitness prediction*
        + *Fine-tuning protein language models boosts predictions across diverse tasks*
        + *ProstT5: Bilingual Language Model for Protein Sequence and Structure*

- Time series representation: (I have only parsed these papers, I'd be happy to have someone take them :) )
    + Start with: *Anomaly Detection Using Multiscale Signatures*
    + Then you may want to consider the more mathematical: *Principal geodesic analysis for time series encoded with signature features*
    + Here it's more about feature building, but there are interesting things to explain, as DTW, signatures, and the geometry of the manifold of signatures (explained in the second paper)
    + You may be able to make small bridge with  *MACE: Higher Order Equivariant Message Passing Neural Networks for Fast and Accurate Force Fields* if someone else takes te topic *Rotation Equivariant networks (E(3) or SE(3))*. Maybe you won't see the link that I see.

- Optimal Transport - Wasserstein distance (rather theoretical subject)
    + You can start by understanding the wasserstein (or earth-mover) distance between distributions.
    + For this you can look anywhere by yourself and/or download the book by Peyré & Cuturi: https://arxiv.org/abs/1803.00567
    + An example of application of this distance for ML is : *DeepJDOT: Deep Joint Distribution Optimal Transport for Unsupervised Domain Adaptation.pdf* It would be good to discuss it, at least the high-level (meta-description) ideas that are used there.
    + Another example would be GROMOV-WASSERSTEIN AUTOENCODERS

### Interesting (Deep) architectures (more generic topics)

- Low-Rank Adaptation (LoRA) **Assuming someone else does the Transformers**, so you can present after them.
    + *LoRA: Low-Rank Adaptation of Large Language Models* (historical paper)
    + A theory but seemingly not too diffcult paper (at first glance): *The Expressive Power of Low-Rank Adaptation*
    + Explain the idea of LoRA: initial motivation (why it's needed), how it works in practice, and then the inconvenients and solutions for those.

- (restricted) Boltzman Machines  (is kind of between an archi and theory of learning)
    + You can start from the Mehta, which is quite pedagogical, describes the setup and the equations. After understanding the learning procedure, you may discuss this kind of method in general (which is a kind of Energy-Based Model.. but most EBM are actually an RBM, I think !)
    + additional reads, to dig a bit deeper, about how learning RBM is out-of-equlibrium , and it's better to infer also out of equilibrium:
        + experimental paper (2021) *Equilibrium and non-Equilibrium regimes in the learning of Restricted Boltzmann Machines*  Part of Advances in Neural Information Processing Systems 34 (NeurIPS 2021)
        + theory paper (2023): *Explaining the effects of non-convergent MCMC in the training of Energy-Based Models*

- Contrastive Language-Image Pre-training (CLIP) :
    + *Demystifying CLIP Data*
    + Explain the ideas of CLIP, zero-shot learning, and whatever is needed to understand CLIP and/or this ICLR 2024 paper, *Demystifying CLIP Data*

- Normalizing Flows
    + This may be technical but it's also one of the recent hype stuff ! :D (in 2022-23 at least)
    + This paper is pretty good (JMLR is an excellent venue). The first 10 pages are introductory and a bit abstract.
    + You may focus on part 3.(Constructing Flows part I) and focus on one or two kind of flows (one of the sub-sections)
    + If it helps, you can of course use other references (there may be more pedagogical tutorials, I don't know !)


## ML for Science

- Conditional Probabilistic Generative models + Wavelets
    + Summarize one or two of these works by Mallat. You should probably do a short introduction on wavelet decomposition, because.. it's pretty cool. Once wavelets are explained, you can focus on how they are used in these papers.
    + *Multiscale Data-Driven Energy Estimation and Generation*
    + *Wavelet conditional renormalization group*
    + *Wavelet Score-Based Generative Modeling*
    + These papers are at the overlap between good ML and good physics (and, use wavelets, of course... since Mallat is the king of wavelets)
    + This is somewhat related and complementary to the other subject, "Scale equivariant networks".

- Deep for PDEs:
    *Deep Statistical Solvers* : https://proceedings.neurips.cc/paper/2020/hash/5a16bce575f3ddce9c819de125ba0029-Abstract.html

## Theory of learning:

#### Note: some are theoretical, some have also en empirical part (thus simpler to understand)

- Growing networks: you can pick and choose one of these 4, or cover and compare some/all of them (required if you are a group of 2 persons):
    + *NeST: A Neural Network Synthesis Tool Based on a Grow-and-Prune Paradigm*
    + *When, where, and how to add new neurons to ANNs* (NORTH*)
    + *Firefly Neural Architecture Descent: a General Approach for Growing Neural Networks*
    + *Growing Tiny Networks: Spotting Expressivity Bottlenecks and Fixing Them Optimally* (disclaimer: I know this one very well, I'm working with these guys) https://arxiv.org/abs/2405.19816
    slightly less on topic, somehow:
    + *GradMax: Growing Neural Networks using Gradient Information*

- ICA and *Robustness of Nonlinear Representation Learning*
    + *Robustness of Nonlinear Representation Learning*
    + A recent paper by the famous B. Schölkopf.
    + They use some known results from material science
    + Goal: summarize the paper, trying to pinpoint the key ideas and results (you don't have to cover 100% of the paper, it has many theorems)
    + Note: you should first understand (and explain at the oral) what is ICA, and how it works in practice.

- Class Imbalance (related to the other "Class Imbalance" project) -- but here it's more theoretical
    + Emanuele Loffredo, Mauro Pastore, Simona Cocco, Rémi Monasson *Restoring balance: principled under/oversampling of data for optimal classification*
    + Disclaimer: I have a theoretical paper on Class Imbalance accepted at AISTATS: https://arxiv.org/abs/2501.11638

- Theory of diffusion models:
    + *Dynamical regimes of diffusion models*  Giulio Biroli, Tony Bonnaire, Valentin de Bortoli & Marc Mézard
        https://www.nature.com/articles/s41467-024-54281-3
    + You should probably read some introductory, pedagogical  material on Diffusion models, that you would also quickly present in your oral presentation

- NTK: Neural Tangent Kernel.
    + Todo, for you: *find some refs* (by yourself, for now)
    + I'm happy to have someone summarize the idea and results about NTK. And its limitations !!

- Grokking:
    + *Grokking: Generalization Beyond Overfitting on Small Algorithmic Datasets* (seminal paper)
    + *Towards Understanding Grokking: An Effective Theory of Representation Learning* (a nice study)
    + Explain a bit grokking, first empirically, then summarize the theory(ies) that attempt to explain it, and conclude with your own opnion or what is "future work"

- Double Descent: (older, simpler topic -- I expect a very sharp, precise and well done presentation)
    + Here you can start from Francis Bach's notes (he has papers on the subject too, but they may be quite mathemtical - as in, theorems and rigorous proofs !)
    + The paper from the physicists should be quite understandable.
    + You can start from these 2 papers but it is encouraged to dig further and read also more recent papers on the topic
    + in summary, explaining the empirical observation (double descent), its interpretation, and the context of it (what is the lazy regime, in particular, and what are other learning regimes) will make for a good presentation

- Learning landscape vs Spin glasses
    + you can choose a single paper among these (in the folder), or propose another one on that topic if you find a better/simpler one

- Tensor PCA (theoretical subject)
    + Here I have just found an example, but there are many
    + You can search for other papers, maybe find simpler ones. A paper describing learning dynamics in any teacher-student scenario will do.

## General or transverse concepts in ML/DL:

- Class Imbalance (related to the other "Class Imbalance" project) -- but here it's more applied
    + Summarize these 3 papers (or part of these 3), that propose tools to deal with the pbm of class imbalance, empirically (with more or less sound theoretical justifications)
    + *Learning Imbalanced Datasets with Label-Distribution-Aware Margin Loss*
    + *AutoBalance: Optimized Loss Functions for Imbalanced Data*
    + *LONG-TAIL LEARNING VIA LOGIT ADJUSTMENT*
    + Of course, you should try to nake sense of the results, interpret a bit the phenomenology.
    + Disclaimer: I have a theoretical paper on Class Imbalance accepted at AISTATS: https://arxiv.org/abs/2501.11638

- Calibration of Neural Networks:
    + You should introduce the topic, i.e. why we need calibration, then cover some solutions on hwo to address it. A starting point could be publications by Lucas Clarté:
    + *Theoretical characterization of uncertainty in high-dimensional linear classification*
    + Or *Expectation consistency for calibration of neural networks*
    + Those may be a bit theoretical, but hopefully not too techcnical.

- AutoML/AutoDL:
    + DeepHyper: https://deephyper.readthedocs.io/en/latest/tutorials/index.html
    To understand it, I recommend:
    + check out the PhD:
        *Optimization of Learning Workflows at Large Scale on High-Performance Computing Systems*
    + you may want to look at:
        *A Tutorial on Bayesian Optimization* (Frazier), or another one that you like better.
    + In short: focus on how DeepHyper works, in principle, and what it yields, in practice.

- Loss functions zoo:
    + *Crack segmentation of imbalanced data: The role of loss functions*
    + Only valid for a single-person "group"
    + you probably need to do more than summarizing the paper, to make it into an interesting presentation. As I recall, there were maybe a few intersting observations in this paper.

- Kolmogorov-Arnold Networks (KANs) (if you really want it, but I'd advise not to take this subject)
    + *https://en.wikipedia.org/wiki/Kolmogorov%E2%80%93Arnold_representation_theorem* (initial theorem)
    + *KAN: Kolmogorov–Arnold Networks* (2024 "original" paper)
    + *KAN or MLP: A Fairer Comparison* -> an example of more reasobale people
    + Here an idea would be to tell the story, from the old theorem, to the 2024' paper *KAN: Kolmogorov–Arnold Networks* and the intense hype around KANs that arose back then (as if they were the new transformers), and back to reality with more recent, more nuanced papers. It's half science and half history of science (although all this happened in 2024).

### the remaining 3 topics in this section are a bit old/well covered on internet -- be careful if you pick them.. you need to master the subject well (not just ChatGPT)  -- basically I don't recommend these subjects.. contact me especially about them, to see what intersting thing you could propose about them

- Ensemble methods
    + you are free to search for *tutorials on ensemble methods* and give us a lecture about it ! :)
    + I expect you would present the case of random forests, which are a good example of success of ensemble methods
    + there are also ensemble of Deep models (!)

- Transfer Learning
    + Here you should understand all and try to summarize to class.
    + You may try to apply it on a simple case (e.g. CIFAR-10 vs ImageNet with a pre-trained nets - see HuggingFace or similar websites).
    + You can browse the litterature yourself for reviews: I just download the first 2 papers that came out of a 10-seconds search.
    + more advanced read: Challenging Common Assumptions in the Unsupervised Learning of Disentangled Representations.pdf
    + more advanced read: GPT3 - Transfer Learning -NeurIPS-2020-language-models-are-few-shot-learners-Paper.pdf

- Boosting methods - Adaboost
    + This is the classic paper + a paper from 2015.
    + Here you should explain the general ideas (momentum, boosting) to the whole class, and try to code a simple method from scratch.

- xgBoost:
    + XGBoost is a famously efficient alogrithm that relies on trees and boosting.
    + Here you can use one of the three papers proposed (the one you prefer) to explain us what is boosting, in the particular case of Decision or Regression Trees (one of them is enough)

These last 2 are similar (AdaBoost and XGBoost), so only one can be taken.


- Attention - transformers **But I will actually do it in session 7**
    + The famous *attention is all you need* paper is a good read, not too long.
    + Then you are free to find other resources to give us a class on attention/transformers, not necessarily applied to text data.
    + For instance you can consider visual attention (attention in CNNs)
    + I expect you to give a precise mathematical definition of at least one attention mechanism, the pros and drawbacks of attention, and most importantly the flavor of the general idea behind attention, with nice sketches. Also, you should talk about the associated compute cost (how they scale with context size, etc.)

## Others (misc.):

- *Monitoring AI-Modified Content at Scale: A Case Study on the Impact of ChatGPT on AI Conference Peer Reviews*
    + Here you should explain the paper, the results, but also outline the limitations of the methodology used in the paper.

- Understanding Black-box Predictions via Influence Functions.pdf
    + There may be other good papers about saliency analysis / ablation studies / [methods that help figure out how predictions are made]

- * - Probably more difficult ones
    Here a folder where the reads may prove more difficult.
