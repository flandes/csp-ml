Given your number, in the end presentations will be of duration 10 min presentation + 7 min question /switch to another presentation.
Try to keep your timing. I'll have to stop you at 11-12 min.

About content:
the goals of the project are multiple:
- have you study a topic deeply, while learning about something new. That's obvious.
- learn to synthesize information to a broad audience in a nice way
- corollary: have classmates also learn about your topic !
I want to insist on clarity. When you prepare, try to pretend that you are graded by the class, not me. You should be clear and interesting to your classmates, then it's a good presentation. Don't try to impress me with technical things, try instead to focus on the few (new) core ideas of the topic you researched, and summarize them down to a few ideas, that should sound simple (provided the context needed). Skip technical details that are not part of the core thing, or that can be discarded as "oh yes and there all these technical details, but that's tedious and one can do it".
Maths are welcomed of course, but try to keep only the necessary stuff, to make ideas clearer, not to obscure them.
It's hard to make things simple, it's easy to make them hard.
Also, keep a critical eye on your readings. You are very welcome to point out limitations or failings of the papers you read. Taking the authors' words for granted is not what is expected from scientists.

Kind of similar to the earlier piece of advice:
- cover little, but well. It's better to cover a rather narrow subject, but really well, than do a broad overview of many many things, but superficially.

If instructions on how to handle (like, what do I expect from you on that particular topic) are not detailed enough, please let me know. This could also happen that you read quite a lot, and then need more precise instructions. Don't hesitate to ask me by e-mail. I will then narrow down the subject.

I can review your slides/ code/ short report (optional), but I need a couple of days. So, if you want feedback early enough, send me your material early enough.


About my questions - a small warning:
Although the exam consists in presenting one of the topics I propose, corresponding typically to one or a couple of papers to read/summarize/re-code/etc (depending on the topic), a large fraction of the grade comes from your ability at answering my questions during the discussion time.
I ask questions assuming that you have followed the classes. It happens very often that I ask for further clarifications on a technical point of the paper, which corresponds to a concept seen in class. So, although in principle being solid on your reading is enough to get a good mark, typically, having followed all classes can help a lot improvising a (good) answer, when it happens that you overlooked a concept during the preparation of your presentation.
